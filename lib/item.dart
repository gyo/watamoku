class Item {
  Item(this.text, this.isWrong);

  final String text;
  final bool isWrong;

  bool isChecked = false;
}
