import "package:flutter/material.dart";
import 'package:watamoku/item.dart';

class ResultPage extends StatelessWidget {
  final List<Item> items;

  ResultPage({Key key, @required this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final irRight = this.items.every((item) {
      return item.isWrong == item.isChecked;
    });

    return new Scaffold(
      appBar: new AppBar(title: new Text("モクシング結果")),
      body: new Center(
        child: irRight ? new Text("完璧です！") : new Text("惜しい！")
      )
    );
  }
}
