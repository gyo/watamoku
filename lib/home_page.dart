import "package:flutter/material.dart";

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("わたしのモクシング")),
      body: new Center(
        child: new RaisedButton(
          onPressed: () {
            Navigator.pushNamed(context, '/list');
          },
          child: new Text('サンプル スタート'),
        )
      ),
    );
  }
}
