import "package:flutter/material.dart";
import 'package:watamoku/home_page.dart';
import 'package:watamoku/list_page.dart';

void main() => runApp(new App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Flutter Demo",
      theme: new ThemeData(
        primaryColor: Colors.brown
      ),
      home: new HomePage(),
      onGenerateRoute: (routeSettings) {
        var path = routeSettings.name.split('/');

        if (path[1] == "list") {
          return new MaterialPageRoute(
            builder: (context) => new ListPage(),
            settings: routeSettings,
          );
        }
      },
    );
  }
}
