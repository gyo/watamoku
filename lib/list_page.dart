import "package:flutter/material.dart";
import 'package:watamoku/item.dart';
import 'package:watamoku/result_page.dart';

class ListPage extends StatefulWidget {
  @override
  ListPageState createState() => new ListPageState();
}

class ListPageState extends State<ListPage> {
  final items = new List<Item>.generate(30, (i) => new Item("$i", i % 5 == 0));

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("5の倍数を選べ"),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.check),
            onPressed: () {
              Navigator.push(
                context,
                new MaterialPageRoute(
                  builder: (context) {
                    return new ResultPage(
                      items: items
                    );
                  }
                )
              );
            }
          )
        ],
      ),
      body: new ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return new ListTile(
            title: new Text("${items[index].text}"),
            trailing: new Icon(
              Icons.check,
              color: items[index].isChecked ? Colors.green : Colors.white
            ),
            onTap: () {
              setState(() {
                items[index].isChecked = !items[index].isChecked;
              });
            }
          );
        }
      )
    );
  }
}
